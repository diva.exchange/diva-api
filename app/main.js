/**
 * Copyright (C) 2020 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import fs from 'fs-extra'
import { DivaApi } from './src/diva-api'
import { Logger } from '@diva.exchange/diva-logger'
import path from 'path'
import process from 'process'

const config = _configure()
Logger.trace('Configuration').trace(config)

DivaApi.make(config)

process.once('SIGINT', () => {
  DivaApi.shutdown().then(() => {
    process.exit(0)
  })
})

function _configure () {
  let _p
  const config = require('../package.json')[process.env.NODE_ENV === 'production' ? 'DivaApi' : 'devDivaApi']

  process.env.LOG_LEVEL = config.log_level = process.env.LOG_LEVEL || config.log_level ||
    (process.env.NODE_ENV === 'production' ? 'info' : 'trace')
  Logger.setOptions({ name: config.log_name || 'DivaApi', level: config.log_level })

  // environment has precedence over config
  config.ip_listen = process.env.IP_LISTEN || config.ip_listen
  if (!config.ip_listen) {
    throw new Error('invalid ip')
  }

  config.port_listen = process.env.PORT_LISTEN || config.port_listen
  _p = Math.floor(Number(config.port_listen || 0))
  if (_p < 1025 || _p > 65535) {
    throw new Error('invalid port')
  }
  config.port_listen = _p

  config.i2p_hostname = process.env.I2P_HOSTNAME || config.i2p_hostname
  if (!config.i2p_hostname) {
    throw new Error('invalid i2p hostname')
  }

  config.i2p_port_http_proxy = process.env.I2P_HTTP_PROXY_PORT || config.i2p_port_http_proxy
  _p = Math.floor(Number(config.i2p_port_http_proxy || 0))
  if (_p < 1025 || _p > 65535) {
    throw new Error('invalid i2p http proxy port')
  }
  config.i2p_port_http_proxy = _p

  config.i2p_port_webconsole = process.env.I2P_WEBCONSOLE_PORT || config.i2p_port_webconsole
  _p = Math.floor(Number(config.i2p_port_webconsole || 0))
  if (_p < 1025 || _p > 65535) {
    throw new Error('invalid i2p webconsole port')
  }
  config.i2p_port_webconsole = _p

  config.path_iroha = process.env.PATH_IROHA || config.path_iroha || '/tmp/iroha/'
  if (!fs.pathExistsSync(config.path_iroha) ||
    !fs.pathExistsSync(path.join(config.path_iroha, 'data/name.peer')) ||
    !fs.pathExistsSync(path.join(config.path_iroha, 'data/blockchain.network'))) {
    throw new Error('invalid iroha path')
  }

  config.torii = process.env.TORII || config.torii

  config.creator = process.env.CREATOR || config.creator || false
  const pathData = path.join(config.path_iroha, 'data')
  if (!config.creator) {
    const np = fs.readFileSync(path.join(pathData, 'name.peer')).toString().trim()
    const bn = fs.readFileSync(path.join(pathData, 'blockchain.network')).toString().trim()
    config.creator = np + '@'  + bn
  }
  try {
    fs.readFileSync(path.join(pathData, `${config.creator}.priv`))
  } catch (error) {
    throw new Error('cannot access iroha keys')
  }

  config.bootstrap_peer = config.bootstrap_peer || []
  config.api_endpoint = process.env.API_ENDPOINT || config.api_endpoint || ''
  config.array_peer_endpoint = config.array_peer_endpoint || []

  config.API_BUILD = (new Date(fs.statSync(path.join(__dirname, '../node_modules')).mtime)).toISOString()

  return config
}
