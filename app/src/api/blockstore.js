/**
 * Copyright (C) 2020 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import fs from 'fs-extra'
import path from 'path'

export class Blockstore {
  /**
   * @param res
   * @param pathIroha {string}
   * @returns {Promise<void>}
   */
  static get (res, pathIroha) {
    return new Promise((resolve, reject) => {
      const pathBlockstore = path.join(pathIroha, 'export/blockstore.tar.xz')
      if (fs.existsSync(pathBlockstore)) {
        res.setHeader('Content-Disposition', 'attachment; filename=blockstore.tar.xz')
        res.setHeader('Content-Type', 'application/octet-stream')
        res.setHeader('Content-Length', fs.statSync(pathBlockstore).size)
        fs.createReadStream(pathBlockstore).pipe(res)
        resolve()
      } else {
        reject(404)
      }
    })
  }
}

module.exports = { Blockstore }
