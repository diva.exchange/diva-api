/**
 * Copyright (C) 2020 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import { DivaApi } from '../diva-api'
import { Iroha } from '../iroha'
import { Logger } from '@diva.exchange/diva-logger'

/**
 * @typedef typeApplyAccount
 * @property {string} api
 * @property {string} account
 * @property {string} key
 *
 * @typedef typeCreateAccount
 * @property {string} address
 * @property {string} account
 * @property {string} key
 *
 * @typedef typeConfirmAccount
 * @property {string} account
 * @property {string} key
 */
export class Account {
  /**
   * @param searchParams
   * @returns {Promise<typeApplyAccount|number>}
   */
  static apply (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        const api = Iroha.validatePeer(searchParams.get('api'))
        const address = DivaApi.apiEndpoint
        const account = Iroha.validateAccount(searchParams.get('account'))
        const key = Iroha.validateKey(searchParams.get('key'))

        const url = `http://${api}/account/create?address=${address}&account=${account}&key=${key}`
        DivaApi.tunnelGet(url)
          .then(() => {
            Logger.info(`Account.apply: applied to create ${account} at ${api}`)
            DivaApi.setOwner('account:' + account, { key: key, timestamp: Math.floor(Date.now() / 1000) })
            resolve({ api: api, account: account, key: key })
          })
          .catch(() => {
            Logger.warn(`Account.apply: failed to apply to create ${account} at ${api}`)
            reject(503)
          })
      } catch (error) {
        Logger.warn('Account.apply failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param searchParams
   * @returns {Promise<typeCreateAccount|number>}
   */
  static create (searchParams) {
    return new Promise((resolve, reject) => {
      if (!DivaApi.iroha) {
        return reject(503)
      }

      try {
        const address = Iroha.validatePeer(searchParams.get('address'))
        const account = Iroha.validateAccount(searchParams.get('account'))
        const key = Iroha.validateKey(searchParams.get('key'))
        Logger.info(`Account.create: request to create ${account}`)
        resolve({ address: address, account: account, key: key })
      } catch (error) {
        Logger.warn('Account.create failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param address {string}
   * @param account {string}
   * @param key {string}
   */
  static sendConfirm (address, account, key) {
    const url = `http://${address}/account/confirm?account=${account}&key=${key}`
    DivaApi.tunnelGet(url)
      .then(() => {
        const [nameAccount, idDomain] = account.split('@')
        DivaApi.iroha.createAccount(nameAccount, idDomain, key)
          .then(() => {
            Logger.info(`iroha.createAccount: ${account}`)
          })
          .catch((error) => {
            Logger.warn(`iroha.createAccount failed: ${account}`).trace(error)
          })
      })
      .catch((error) => {
        Logger.warn(`Account.sendConfirm: requesting ${url} failed`).trace(error)
      })
  }

  /**
   * @param searchParams
   * @returns {Promise<typeConfirmAccount|number>}
   */
  static confirm (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        const account = Iroha.validateAccount(searchParams.get('account'))
        const key = Iroha.validateKey(searchParams.get('key'))

        const lookup = 'account:' + account
        const obj = DivaApi.isOwner(lookup)
        if (!obj) {
          Logger.warn(`Account.confirm: ${lookup} not found`)
          reject(404)
        } else if (obj.key === key) {
          Logger.info(`Account.confirm: ${lookup} confirmed`)
          resolve({ account: account, key: key })
        } else {
          Logger.warn(`Account.confirm: ${lookup} failed`)
          reject(403)
        }
      } catch (error) {
        Logger.warn('Account.confirm failed').trace(error)
        reject(403)
      }
    })
  }
}

module.exports = { Account }
