/**
 * Copyright (C) 2020 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import { DivaApi } from '../diva-api'
import fs from 'fs-extra'
import path from 'path'

export class Block {
  /**
   * @param searchParams
   * @returns {Promise<any>}
   */
  static get (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        let id = searchParams.get('id')
        id = Number(id) ? id.toString().padStart(16, '0') : ''
        const pathLatest = path.join(DivaApi.pathIroha, 'export/latest')
        if (!id && fs.existsSync(pathLatest)) {
          id = fs.readFileSync(pathLatest).toString().trim()
        }
        if (id) {
          const pathBlock = path.join(DivaApi.pathIroha, 'blockstore', id)
          if (fs.existsSync(pathBlock)) {
            return resolve(JSON.parse(fs.readFileSync(pathBlock)))
          }
        }
        reject(404)
      } catch (error) {
        reject(403)
      }
    })
  }
}

module.exports = { Block }
