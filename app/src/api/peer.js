/**
 * Copyright (C) 2020 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import { DivaApi } from '../diva-api'
import { Iroha } from '../iroha'
import { Logger } from '@diva.exchange/diva-logger'

/**
 * @typedef typeGetPeer
 * @property {string} address
 * @property {string} key
 *
 * @typedef typeApplyPeer
 * @property {string} action
 * @property {string} api
 * @property {string} address
 * @property {string} key
 *
 * @typedef typeModifyPeer
 * @property {string} action
 * @property {string} address
 * @property {string} key
 *
 * @typedef typeConfirmPeer
 * @property {string} action
 * @property {string} address
 * @property {string} key
 */
export class Peer {
  /**
   * @param searchParams
   * @returns {Promise<typeGetPeer|number>}
   */
  static get (searchParams) {
    return new Promise((resolve, reject) => {
      if (!DivaApi.iroha) {
        return reject(503)
      }

      try {
        const address = Iroha.validatePeer(searchParams.get('address'))
        DivaApi.iroha.getPeers()
          .then((peers) => {
            for (const key in Object.values(peers)) {
              if (peers[key].address === address) {
                return resolve({
                  address: address,
                  key: peers[key].peerKey
                })
              }
            }
            reject(404)
          })
          .catch((error) => {
            Logger.warn('iroha.getPeers failed').trace(error)
            reject(500)
          })
      } catch (error) {
        Logger.warn('Peer.get failed').trace(error)
        reject(403)
      }
    })
  }

  // @FIXME obsolete and a major security issue
  /**
   * @param searchParams
   * @returns {Promise<any>}
   */
  static remove (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        const key = Iroha.validateKey(searchParams.get('key'))
        DivaApi.iroha.removePeer(key)
          .then((json) => resolve(json))
          .catch(() => reject(404))
      } catch (error) {
        Logger.warn('Peer.remove failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param searchParams
   * @returns {Promise<typeApplyPeer|number>}
   */
  static apply (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        const action = Peer._validateAction(searchParams.get('action'))
        const api = Iroha.validateHostname(searchParams.get('api'))
        const address = Iroha.validatePeer(searchParams.get('address'))
        const key = Iroha.validateKey(searchParams.get('key'))

        const url = `http://${api}/peer/modify?action=${action}&address=${address}&key=${key}`
        DivaApi.tunnelGet(url)
          .then(() => {
            Logger.info(`Peer.apply: applied to ${action} ${address} at ${api}`)
            DivaApi.setOwner(action + ':' + address, { key: key, timestamp: Math.floor(Date.now() / 1000) })
            resolve({ action: action, api: api, address: address, key: key })
          })
          .catch(() => {
            Logger.warn(`Peer.apply: failed to apply to ${action} ${address} at ${api}`)
            reject(503)
          })
      } catch (error) {
        Logger.warn('Peer.apply failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param searchParams
   * @returns {Promise<typeModifyPeer|number>}
   */
  static modify (searchParams) {
    return new Promise((resolve, reject) => {
      if (!DivaApi.iroha) {
        return reject(503)
      }

      try {
        const action = Peer._validateAction(searchParams.get('action'))
        const address = Iroha.validatePeer(searchParams.get('address'))
        const key = Iroha.validateKey(searchParams.get('key'))
        Logger.info(`Peer.modify: request to ${action} ${address}`)
        resolve({ action: action, address: address, key: key })
      } catch (error) {
        Logger.warn('Peer.modify failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param action {string}
   * @param address {string}
   * @param key {string}
   */
  static sendConfirm (action, address, key) {
    const url = `http://${address.replace(/:[\d]+$/i, '')}/peer/confirm?action=${action}&address=${address}&key=${key}`
    DivaApi.tunnelGet(url)
      .then(() => {
        switch (action) {
          case 'add':
            DivaApi.iroha.addPeer(address, key)
              .then(() => {
                Logger.info(`iroha.addPeer: ${address}`)
              })
              .catch((error) => {
                Logger.warn(`iroha.addPeer failed: ${address}`).trace(error)
              })
            break
          case 'remove':
            DivaApi.iroha.removePeer(key)
              .then(() => {
                Logger.info(`iroha.removePeer: ${address}`)
              })
              .catch((error) => {
                Logger.warn(`iroha.removePeer failed: ${address}`).trace(error)
              })
            break
        }
      })
      .catch((error) => {
        Logger.warn(`Peer.sendConfirm: requesting ${url} failed`).trace(error)
      })
  }

  /**
   * @param searchParams
   * @returns {Promise<typeConfirmPeer|number>}
   */
  static confirm (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        const action = Peer._validateAction(searchParams.get('action'))
        const address = Iroha.validatePeer(searchParams.get('address'))
        const key = Iroha.validateKey(searchParams.get('key'))

        const lookup = action + ':' + address
        const obj = DivaApi.isOwner(lookup)
        if (!obj) {
          Logger.warn(`Peer.confirm: ${lookup} not found`)
          reject(404)
        } else if (obj.key === key) {
          Logger.info(`Peer.confirm: ${lookup} confirmed`)
          resolve({ action: action, address: address, key: key })
        } else {
          Logger.warn(`Peer.confirm: ${lookup} failed`)
          reject(403)
        }
      } catch (error) {
        Logger.warn('Peer.confirm failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param action {string}
   * @returns {string}
   * @private
   */
  static _validateAction (action) {
    switch (action) {
      case 'add':
      case 'remove':
        break
      default:
        throw new Error('invalid action')
    }
    return action
  }
}

module.exports = { Peer }
