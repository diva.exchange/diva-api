/**
 * Copyright (C) 2020 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import { Account } from './api/account'
import { Block } from './api/block'
import { Blockstore } from './api/blockstore'
import { Message } from './api/message'
import { Peer } from './api/peer'
import crypto from 'crypto'
import fs from 'fs-extra'
import get from 'simple-get'
import http from 'http'
import { Iroha } from './iroha'
import { IrohaDb } from './iroha-db'
import { Logger } from '@diva.exchange/diva-logger'
import path from 'path'
import tunnel from 'tunnel'
import WebSocket from 'ws'

const VERSION = '0.1.0-alpha'
const TIMEOUT_REQUEST_TUNNEL_MS = 30000 // 30 secs

const I2P_API_ADDRESS_FILTER = 'api-'

const URL_I2P_PING_DEFAULT = 'http://diva.i2p/ping'

const MS_1M = 60000
const MS_3M = 180000
const MS_5M = 300000

/**
 * @typedef typeConfig
 * @property {string} API_BUILD
 * @property {string} ip_listen
 * @property {number} port_listen
 * @property {string} i2p_hostname
 * @property {number} i2p_port_http_proxy
 * @property {number} i2p_port_webconsole
 * @property {?array} bootstrap_peer
 * @property {string} path_iroha
 * @property {string} torii
 * @property {string} creator
 * @property {string} api_endpoint
 * @property {array} array_peer_endpoint
 * @property {string} log_name
 * @property {string} log_level
 */
export class DivaApi {
  /**
   * Singleton, static init
   *
   * @param config {typeConfig}
   */
  static make (config) {
    // API token
    DivaApi._token = crypto.randomBytes(32).toString('hex')
    fs.writeFileSync(path.join(__dirname, '../../data/token'), DivaApi._token)

    DivaApi.version = VERSION + '-' + config.API_BUILD
    DivaApi._ip = config.ip_listen
    DivaApi._port = config.port_listen
    DivaApi._hostnameI2P = config.i2p_hostname
    DivaApi._portHttpProxyI2P = config.i2p_port_http_proxy
    DivaApi._portWebconsoleI2P = config.i2p_port_webconsole

    DivaApi.iroha = null
    DivaApi.irohaDb = null
    DivaApi.pathIroha = config.path_iroha
    DivaApi.torii = config.torii
    DivaApi.creator = config.creator
    DivaApi.pkCreator = ''
    DivaApi.namePeer = ''
    DivaApi.pkPeer = ''

    DivaApi.bootstrapPeer = config.bootstrap_peer
    DivaApi.peerEndpoint = new Map(config.array_peer_endpoint)
    DivaApi.apiEndpoint = config.api_endpoint

    if (DivaApi.apiEndpoint) {
      Logger.info(`HTTP API ${DivaApi.apiEndpoint}`)
    }
    if (DivaApi.peerEndpoint.size) {
      DivaApi.peerEndpoint.forEach((v, k) => {
        Logger.info(`Peer ${k}, ${v}`)
      })
    }

    DivaApi._owner = new Map()

    DivaApi._initEndpoint()
    DivaApi._initServer()
    DivaApi._initIroha()
    DivaApi._cleanOwnership()
  }

  /**
   * @private
   */
  static _initEndpoint () {
    if (DivaApi.apiEndpoint) {
      return
    }

    Logger.info('Testing network connectivity...')
    DivaApi.tunnelGet(URL_I2P_PING_DEFAULT).then(() => {
      Logger.info('Network available. Looking for API endpoints...')

      // scrape local API endpoints
      const options = {
        url: `http://${DivaApi._hostnameI2P}:${DivaApi._portWebconsoleI2P}/?page=i2p_tunnels`
      }
      get.concat(options, (error, response, data) => {
        if (error) {
          Logger.warn('Could not access I2P webconsole - will retry in 1min').trace(error)
          DivaApi._timeout('init', () => { DivaApi._initEndpoint() }, MS_1M)
          return
        }
        const reS = /Server Tunnels:(.+)$/s
        const m = reS.exec(data.toString())
        if (m && m[1]) {
          const reB32 = /b32=[^>]*>([^<]+).+?(([a-z0-9]+\.b32\.i2p)(:[\d]+)?)/g
          const arrayB32 = [...m[1].matchAll(reB32)]
          if (arrayB32.length) {
            arrayB32.forEach((a) => {
              if (!DivaApi.apiEndpoint && a[1].indexOf(I2P_API_ADDRESS_FILTER) === 0) {
                DivaApi.apiEndpoint = a[3]
                Logger.info(`HTTP API ${DivaApi.apiEndpoint}`)
              }
              if (a[1].indexOf(I2P_API_ADDRESS_FILTER) === -1) {
                DivaApi.peerEndpoint.set(a[2], a[1])
                Logger.info(`Peer ${a[2]}, ${a[1]}`)
              }
            })
          }
        }
        if (!DivaApi.apiEndpoint || !DivaApi.peerEndpoint.size) {
          throw new Error('No endpoints found')
        }
      })
    }).catch(() => {
      Logger.warn('I2P Network not available - will retry in 1min')
      DivaApi._timeout('init', () => DivaApi._initEndpoint(), MS_1M)
    })
  }

  /**
   * @private
   */
  static _initServer () {
    DivaApi._httpServer = http.createServer((req, res) => {
      switch (req.method) {
        case 'GET':
          return DivaApi._processApiGet(req, res)
        case 'POST':
          return DivaApi._processApiPost(req, res)
        default:
          return DivaApi._errorRequest(res, 403)
      }
    })
    DivaApi._httpServer.on('close', () => {
      Logger.info(`API (${DivaApi.version}) closed`)
    })
    DivaApi._httpServer.listen(DivaApi._port, DivaApi._ip, () => {
      Logger.info(`API (${DivaApi.version}) listening on ${DivaApi._ip}:${DivaApi._port}`)
    })

    /** @type WebSocket.Server */
    DivaApi._wss = new WebSocket.Server({ server: DivaApi._httpServer })
    DivaApi._wss.on('connection', (ws, request) => {
      if (!DivaApi._token || !request.headers['diva-auth-token'] || request.headers['diva-auth-token'] !== DivaApi._token) {
        ws.close(1008, 'Not Authorized')
        return
      }

      ws.on('message', async (data) => {
        try {
          const obj = JSON.parse(data)
          await DivaApi._processWebsocket(ws, obj)
        } catch (error) {
          Logger.trace(error)
        }
      })
      ws.on('close', (code, reason) => {
        const msg = reason ? `${code} - ${reason}` : code
        Logger.info(`Websocket closed ${msg}`)
      })
    })
    DivaApi._wss.on('error', (error) => {
      Logger.warn('WebsocketServer error').trace(error)
    })
    DivaApi._wss.on('listening', () => {
      const wsa = DivaApi._wss.address()
      Logger.info(`WebsocketServer listening on ${wsa.address}:${wsa.port}`)
    })
    DivaApi._wss.on('close', () => {
      Logger.info('WebsocketServer closed')
    })
  }

  /**
   * @private
   */
  static _initIroha () {
    (async () => {
      try {
        DivaApi.namePeer = fs.readFileSync(
          path.join(DivaApi.pathIroha, 'data/name.peer')).toString().trim()
        DivaApi.pkPeer = fs.readFileSync(
          path.join(DivaApi.pathIroha, `data/${DivaApi.namePeer}.pub`)).toString().trim()
        DivaApi.pkCreator = fs.readFileSync(
          path.join(DivaApi.pathIroha, `data/${DivaApi.creator}.pub`)).toString().trim()
        DivaApi.iroha = await Iroha.make(DivaApi.torii, DivaApi.creator, DivaApi.pathIroha)
        Logger.info('Iroha ready')
          .info(`Path ${DivaApi.pathIroha}`)
          .info(`Torii ${DivaApi.torii}`)
          .info(`Peer ${DivaApi.namePeer}`)
          .info(`Public Key Peer ${DivaApi.pkPeer}`)
          .info(`Creator ${DivaApi.creator}`)
          .info(`Public Key Creator ${DivaApi.pkCreator}`)

        DivaApi.irohaDb = new IrohaDb(JSON.parse(
          fs.readFileSync(path.join(DivaApi.pathIroha, 'data/config.json')).toString()
        ))

        // registration
        DivaApi._timeout('registerPeer', () => DivaApi._registerPeer(), MS_3M)
        // ping
        DivaApi._timeout('pingBlockchain', () => DivaApi._pingBlockchain(), MS_1M)
        // block watch
        DivaApi.iroha.watch(DivaApi._onBlock, DivaApi._onBlockError)
      } catch (error) {
        Logger.warn('Iroha not ready yet...').trace(error)
        DivaApi._timeout('initIroha', () => DivaApi._initIroha(), MS_1M)
      }
    })()
  }

  /**
   * @returns {string}
   */
  static getVersion () {
    if (!DivaApi.version) {
      throw new Error('DivaApi not initialized')
    }
    return DivaApi.version
  }

  /**
   * @returns {Promise<void>}
   */
  static async shutdown () {
    DivaApi._mapTimeout.forEach((v) => { clearTimeout(v) })

    DivaApi.iroha = null
    DivaApi.irohaDb = null

    if (typeof DivaApi._wss !== 'undefined' && DivaApi._wss) {
      await new Promise((resolve) => {
        DivaApi._wss.clients.forEach((ws) => {
          ws.close(1000, 'Bye')
        })
        DivaApi._wss.close(resolve)
      })
      DivaApi._wss = null
    }

    if (typeof DivaApi._httpServer !== 'undefined' && DivaApi._httpServer) {
      await new Promise((resolve) => { DivaApi._httpServer.close(resolve) })
      DivaApi._httpServer = null
    }
  }

  /**
   * @param key
   * @returns {*|false}
   */
  static isOwner (key) {
    return (DivaApi._owner.get(key) || false)
  }

  /**
   * @param key
   * @param value
   */
  static setOwner (key, value) {
    DivaApi._owner.set(key, value)
  }

  /**
   * Clean up the ownership map (garbage collector)
   *
   * @private
   */
  static _cleanOwnership () {
    // clean DivaApi._owner, garbage collector
    const ts = Math.floor(Date.now() / 1000) - 300 // 300sec/5min ago
    DivaApi._owner.forEach((v, k) => {
      if (v.timestamp < ts) {
        DivaApi._owner.delete(k)
      }
    })

    DivaApi._timeout('gc', () => DivaApi._cleanOwnership(), 120000)
  }

  /**
   * @param block {Object}
   * @private
   */
  static _onBlock (block) {
    DivaApi._wss.clients.forEach((ws) => {
      try {
        ws.send(JSON.stringify({
          channel: 'block',
          command: 'new',
          block: block
        }))
      } catch (error) {
        Logger.warn('_onBlock error').trace(error)
      }
    })
  }

  /**
   * @param error {Object}
   * @private
   */
  static _onBlockError (error) {
    DivaApi._wss.clients.forEach((ws) => {
      try {
        ws.send(JSON.stringify({
          channel: 'block',
          command: 'error',
          block: error
        }))
      } catch (error) {
        Logger.warn('_onBlockError error').trace(error)
      }
    })
  }

  /**
   * @private
   */
  static async _registerPeer () {
    if (!DivaApi.bootstrapPeer.length) {
      return
    }

    await DivaApi.peerEndpoint.forEach(async (namePeer, address) => {
      if (DivaApi.bootstrapPeer.indexOf(namePeer) > -1 || DivaApi.bootstrapPeer.indexOf(address) > -1 ||
        await DivaApi.irohaDb.hasPeer(address) || await DivaApi.irohaDb.hasPeer(namePeer)) {
        return
      }

      Logger.trace(`Registering peer: ${address} (${namePeer})`)
      DivaApi._shuffleBootstrapPeer()
      const api = DivaApi.bootstrapPeer[0]
      const key = DivaApi.pkPeer
      const url = new URL(
        `http://${DivaApi._ip}:${DivaApi._port}/peer/apply?action=add&api=${api}&address=${address}&key=${key}`,
        `http://${DivaApi._ip}:${DivaApi._port}`
      )
      try {
        const json = await Peer.apply(url.searchParams)
        Logger.info(`Applied to register peer ${address}`).trace(json)
      } catch (error) {
        Logger.warn(`Application to register peer failed ${address}`).trace(error)
      }
    })

    DivaApi._timeout('DivaApi._registerPeer', () => DivaApi._registerPeer(), MS_3M)
    DivaApi._timeout('DivaApi._registerAccount', () => DivaApi._registerAccount(), MS_1M)
  }

  /**
   * @private
   */
  static async _registerAccount () {
    if (!DivaApi.bootstrapPeer.length) {
      return
    }

    const accountId = DivaApi.creator
    if (!(await DivaApi.irohaDb.hasAccount(accountId))) {
      Logger.trace(`Registering account: ${accountId}`)

      DivaApi._shuffleBootstrapPeer()
      const api = DivaApi.bootstrapPeer[0]
      const key = DivaApi.pkCreator
      const url = new URL(
        `http://${DivaApi._ip}:${DivaApi._port}/account/apply?&api=${api}&account=${accountId}&key=${key}`,
        `http://${DivaApi._ip}:${DivaApi._port}`
      )
      Account.apply(url.searchParams)
        .then((json) => {
          Logger.info(`Applied to register account ${accountId}`).trace(json)
        })
        .catch((error) => {
          Logger.warn(`Application to register account failed ${accountId}`).trace(error)
        })
    }

    const objAccount = await DivaApi.irohaDb.getAccount(DivaApi.creator)
    try {
      if (objAccount &&
        (!objAccount.data[DivaApi.creator] || DivaApi.apiEndpoint !== objAccount.data[DivaApi.creator].i2p)) {
        await DivaApi.iroha.setAccountDetail(DivaApi.creator, 'i2p', DivaApi.apiEndpoint)
      }
    } catch (error) {
      Logger.warn('iroha.setAccountDetail failed: i2p').trace(error)
    }

    DivaApi._timeout('DivaApi._registerAccount', () => DivaApi._registerAccount(), MS_3M)
  }

  /**
   * @private
   */
  static async _pingBlockchain () {
    try {
      await DivaApi.iroha.setAccountDetail(DivaApi.creator, 'ping', Math.floor(Date.now() / 1000))
    } catch (error) {
      Logger.warn('iroha.setAccountDetail failed: ping').trace(error)
    }

    DivaApi._timeout('pingBlockchain', () => DivaApi._pingBlockchain(), MS_5M)
  }

  /**
   * @param ident {string}
   * @param handler {function}
   * @param msTimeout {number}
   * @private
   */
  static _timeout (ident, handler, msTimeout) {
    if (!DivaApi._mapTimeout) {
      DivaApi._mapTimeout = new Map()
    }
    clearTimeout(DivaApi._mapTimeout.get(ident))
    DivaApi._mapTimeout.set(ident, setTimeout(handler, msTimeout))
  }

  /**
   * @private
   */
  static _shuffleBootstrapPeer () {
    // durstenfeld shuffle
    for (let i = DivaApi.bootstrapPeer.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [DivaApi.bootstrapPeer[i], DivaApi.bootstrapPeer[j]] = [DivaApi.bootstrapPeer[j], DivaApi.bootstrapPeer[i]]
    }
  }

  /**
   * @param url
   * @returns {Promise<*>}
   */
  static tunnelGet (url) {
    return new Promise((resolve, reject) => {
      const options = {
        url: url,
        timeout: TIMEOUT_REQUEST_TUNNEL_MS,
        headers: {
          'User-Agent': 'diva-' + DivaApi.getVersion()
        }
      }
      if (/^http:\/\/[^/?#]+\.i2p(:[\d]+)?([/?#]|$)/.test(url)) {
        options.agent = tunnel.httpOverHttp({
          proxy: {
            host: DivaApi._hostnameI2P,
            port: DivaApi._portHttpProxyI2P,
            timeout: TIMEOUT_REQUEST_TUNNEL_MS,
            headers: {
              'User-Agent': 'diva-' + DivaApi.getVersion()
            }
          }
        })
      }

      get.concat(options, (error, response, data) => {
        if (!error && response.statusCode === 200) {
          resolve(data)
        } else {
          error ? reject(error) : reject(response.statusCode)
        }
      })
    })
  }

  /**
   * @param url
   * @param body {string}
   * @returns {Promise<*>}
   */
  static tunnelPost (url, body) {
    return new Promise((resolve, reject) => {
      const options = {
        url: url,
        timeout: TIMEOUT_REQUEST_TUNNEL_MS,
        headers: {
          'User-Agent': 'diva-' + DivaApi.getVersion()
        },
        body: body
      }
      if (/^http:\/\/[^/?#]+\.i2p(:[\d]+)?([/?#]|$)/.test(url)) {
        options.agent = tunnel.httpOverHttp({
          proxy: {
            host: DivaApi._hostnameI2P,
            port: DivaApi._portHttpProxyI2P,
            timeout: TIMEOUT_REQUEST_TUNNEL_MS,
            headers: {
              'User-Agent': 'diva-' + DivaApi.getVersion()
            }
          }
        })
      }

      get.post(options, (error, response) => {
        if (error) {
          reject(error)
        } else if (response.statusCode === 200) {
          resolve()
        } else {
          reject(response.statusCode)
        }
      })
    })
  }

  /**
   * @param ws {WebSocket}
   * @param obj {Object}
   * @returns {Promise<void>}
   * @throws {Error}
   * @private
   */
  static async _processWebsocket (ws, obj) {
    if (!obj.channel || !obj.command) {
      return
    }

    const ident = obj.channel + ':' + obj.command
    const response = {
      response: obj.id || 0,
      channel: obj.channel,
      command: obj.command
    }

    switch (ident) {
      case 'chat:message':
        if (obj.message && obj.recipient && await DivaApi.irohaDb.hasAccount(obj.recipient)) {
          const objAccount = await DivaApi.irohaDb.getAccount(obj.recipient)
          obj.sender = DivaApi.creator
          await DivaApi.tunnelPost('http://' + objAccount.data[obj.recipient].i2p + '/message', JSON.stringify(obj))
        }
        break
      case 'trade:getorderbook':
        try {
          response.arrayBook = await DivaApi.irohaDb.getOrderBook(
            obj.contract || '',
            obj.type || '',
            obj.accountId || ''
          )
        } catch (error) {
          Logger.warn(`Error ${ident}`).trace(error)
          response.error = error
        }
        ws.send(JSON.stringify(response))
        break
      case 'trade:setorder':
        if (obj.contract && obj.type && obj.accountId) {
          try {
            await DivaApi.iroha.setAccountDetail(DivaApi.creator, obj.key, obj.value)
          } catch (error) {
            Logger.warn(`Error ${ident}`).trace(error)
            response.error = error
          }
          ws.send(JSON.stringify(response))
        }
        break
    }
  }

  /**
   * @param req Request
   * @param res Response
   * @private
   */
  static _processApiGet (req, res) {
    const host = req.headers.host
    const url = new URL(req.url, `http://${host}`)
    const _p = url.pathname.replace(/\/+$/, '')

    switch (_p) {
      case '/about':
        return DivaApi._endRequest(res, {
          version: DivaApi.version,
          peer: DivaApi.namePeer,
          pkPeer: DivaApi.pkPeer,
          creator: DivaApi.creator,
          pkCreator: DivaApi.pkCreator
        })
      case '/accounts':
        return DivaApi.irohaDb.getAccounts()
          .then((json) => { DivaApi._endRequest(res, json) })
          .catch(() => { DivaApi._errorRequest(res, 403) })
      case '/register-ux':
        if (url.searchParams.get('token') !== DivaApi._token) {
          return DivaApi._errorRequest(res, 401)
        }
        try {
          DivaApi._pkUX = Iroha.validateKey(url.searchParams.get('key'))
        } catch (error) {
          return DivaApi._errorRequest(res, 403)
        }
        return DivaApi.iroha.setAccountDetail(DivaApi.creator, 'pk', DivaApi._pkUX)
          .then(() => { DivaApi._endRequest(res, { pk: DivaApi._pkUX }) })
          .catch(() => { DivaApi._errorRequest(res, 500) })
      case '/blockstore':
        return Blockstore.get(res)
          .catch((status) => { DivaApi._errorRequest(res, status) })
      case '/block':
        return Block.get(url.searchParams)
          .then((json) => { DivaApi._endRequest(res, json) })
          .catch((status) => { DivaApi._errorRequest(res, status) })
      case '/peer':
        return Peer.get(url.searchParams)
          .then((json) => { DivaApi._endRequest(res, json) })
          .catch((status) => { DivaApi._errorRequest(res, status) })
      case '/peer/remove':
        // @FIXME should not be available through API
        return Peer.remove(url.searchParams)
          .then((json) => { DivaApi._endRequest(res, json) })
          .catch((status) => { DivaApi._errorRequest(res, status) })
      case '/peer/apply':
        return Peer.apply(url.searchParams)
          .then((json) => { DivaApi._endRequest(res, json) })
          .catch((status) => { DivaApi._errorRequest(res, status) })
      case '/peer/modify':
        return Peer.modify(url.searchParams)
          .then((json) => {
            DivaApi._endRequest(res, json)
            Peer.sendConfirm(json.action, json.address, json.key)
          })
          .catch((status) => { DivaApi._errorRequest(res, status) })
      case '/peer/confirm':
        return DivaApi.confirm(url.searchParams)
          .then((json) => { DivaApi._endRequest(res, json) })
          .catch((status) => { DivaApi._errorRequest(res, status) })
      case '/account/apply':
        return Account.apply(url.searchParams)
          .then((json) => { DivaApi._endRequest(res, json) })
          .catch((status) => { DivaApi._errorRequest(res, status) })
      case '/account/create':
        return Account.create(url.searchParams)
          .then((json) => {
            DivaApi._endRequest(res, json)
            Account.sendConfirm(json.address, json.accountId, json.key)
          })
          .catch((status) => { DivaApi._errorRequest(res, status) })
      case '/account/confirm':
        return Account.confirm(url.searchParams)
          .then((json) => { DivaApi._endRequest(res, json) })
          .catch((status) => { DivaApi._errorRequest(res, status) })
      default:
        return DivaApi._errorRequest(res, 403)
    }
  }

  /**
   * @param req Request
   * @param res Response
   * @private
   */
  static _processApiPost (req, res) {
    const host = req.headers.host
    const url = new URL(req.url, `http://${host}`)
    const _p = url.pathname.replace(/\/+$/, '')

    switch (_p) {
      case '/message':
        return Message.post(req)
          .then((obj) => {
            if (obj.recipient !== DivaApi.creator) {
              return DivaApi._errorRequest(res, 404)
            }
            DivaApi._wss.clients.forEach((ws) => ws.send(JSON.stringify(obj)))
            DivaApi._endRequest(res)
          })
          .catch((status) => { DivaApi._errorRequest(res, status) })
      default:
        return DivaApi._errorRequest(res, 403)
    }
  }

  /**
   * @param res
   * @param json
   * @private
   */
  static _endRequest (res, json = {}) {
    res.setHeader('Content-Type', 'application/json; charset=utf-8')
    res.statusCode = 200
    res.end(json ? JSON.stringify(json) : null)
  }

  /**
   * @param res
   * @param status {Number}
   * @private
   */
  static _errorRequest (res, status) {
    res.statusCode = status
    res.end()
  }
}

module.exports = { DivaApi, VERSION }
