/*!
 * Iroha DB connectivity (world state database)
 * Copyright(c) 2019-2020 Konrad Baechler, https://diva.exchange
 * GPL3 Licensed
 */

'use strict'

import { Client } from 'pg'
import { Logger } from '@diva.exchange/diva-logger'
import { Iroha } from './iroha'

export class IrohaDb {
  /**
   * @param config
   */
  constructor (config) {
    this._config = {
      host: config.database.host,
      port: config.database.port,
      database: config.database['working database'],
      user: config.database.user,
      password: config.database.password
    }
  }

  /**
   * @param sql
   * @param arrayParam
   * @returns {Promise<*>}
   * @private
   */
  async _query (sql, arrayParam) {
    let c, data
    try {
      c = new Client(this._config)
      await c.connect()
      data = await c.query(sql, arrayParam)
    } catch (error) {
      Logger.warn('query failed').trace(error)
    } finally {
      if (c) await c.end()
    }
    return data
  }

  /**
   * @returns {Promise<Array>}
   */
  async getPeers () {
    const data = await this._query('SELECT * FROM peer')
    return data.rowCount ? data.rows : []
  }

  /**
   * @param address {string}
   * @returns {Promise<boolean>}
   */
  async hasPeer (address) {
    try {
      return (await this._query(
        'SELECT * FROM peer WHERE address = $1',
        [Iroha.validatePeer(address)])
      ).rowCount === 1
    } catch (error) {
      Logger.warn('invalid address').trace(error)
      return false
    }
  }

  /**
   * @param id {string}
   * @returns {Promise<boolean>}
   * @throws {Error}
   */
  async hasAccount (id) {
    try {
      return (await this._query(
        'SELECT * FROM account WHERE account_id = $1',
        [Iroha.validateAccount(id)])
      ).rowCount === 1
    } catch (error) {
      Logger.warn('invalid account id').trace(error)
      return false
    }
  }

  /**
   * @returns {Promise<Array>}
   */
  async getAccounts () {
    const data = await this._query('SELECT account_id, data FROM account')
    return data.rowCount > 0
      ? data.rows.map((row) => {
          const obj = row.data[row.account_id] || {}
          return {
            accountId: row.account_id,
            i2p: obj.i2p,
            pk: obj.pk,
            ping: obj.ping
          }
        })
      : []
  }

  /**
   * @param idAccount {string}
   * @returns {Promise<Object|false>}
   */
  async getAccount (idAccount) {
    try {
      const data = await this._query('SELECT * FROM account WHERE account_id = $1',
        [Iroha.validateAccount(idAccount)])
      return data.rowCount === 1 ? data.rows[0] : false
    } catch (error) {
      Logger.warn('invalid account id').trace(error)
      return {}
    }
  }

  /**
   * @param identContract {string} Empty to fetch the markets of all contracts
   * @param type {string} B or A (Bid or Ask), or empty to fetch all
   * @param identAccount {string} Empty to fetch the markets of all accounts
   * @returns {Promise<Array>}
   * @throws {Error}
   */
  async getOrderBook (identContract = '', type = '', identAccount = '') {
    if (identContract === '') {
      identContract = '[A-Z0-9_]{1,32}'
    } else if (!identContract.match(/^[A-Z0-9_]{1,32}$/)) {
      throw new Error('invalid identContract: ' + identContract)
    }
    if (type === '') {
      type = 'BA'
    } else if (!(type === 'B' || type === 'A')) {
      throw new Error('invalid type: ' + type)
    }

    const sql = 'SELECT account_id, REGEXP_MATCHES(data::TEXT, $1, \'g\') AS array FROM account' +
      (identAccount ? ' WHERE account_id = $2' : '')
    const regex = '"ob' + identContract + 't[' + type + ']"[^"]+"([0-9]+);([^"]+)"'
    const data = await this._query(sql, identAccount ? [regex, identAccount] : [regex])

    return Promise.all(data.rows.map(async (row) => {
      return {
        account_id: row.account_id,
        version: parseInt(row.array[0]),
        book: row.array[1]
      }
    }))
  }

  /**
   * @param idAccount {string}
   * @returns {Promise<string>}
   */
  async getPublicKey (idAccount) {
    const data = await this._query('SELECT public_key FROM account_has_signatory WHERE account_id = $1',
      [idAccount])
    return data.rowCount === 1 ? data.rows[0].public_key : ''
  }
}

module.exports = { IrohaDb }
