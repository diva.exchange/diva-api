#!/usr/bin/env bash
#
# Copyright (C) 2020 diva.exchange
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
#

# -e  Exit immediately if a simple command exits with a non-zero status
set -e

NODE_ENV=${NODE_ENV:-production}

IP_LISTEN=${IP_LISTEN:-0.0.0.0}
PORT_LISTEN=${PORT_LISTEN:-19012}

TORII=${TORII:-127.0.0.0:10051} # defaults to void

I2P_HOSTNAME=${I2P_HOSTNAME:-localhost}
I2P_HTTP_PROXY_PORT=${I2P_HTTP_PROXY_PORT:-4444}
I2P_WEBCONSOLE_PORT=${I2P_WEBCONSOLE_PORT:-7070}

# get the private key from the shared folder - otherwise the API cannot access the blockchain
PATH_IROHA=${PATH_IROHA:-/tmp/iroha/}
t=0
while [[ ${t} < 60 && ! -f ${PATH_IROHA}/data/name.peer ]]
do
  ((t+=1))
  sleep 1
done
[[ ! -f ${PATH_IROHA}/data/name.peer ]] && exit 2
[[ ! -f ${PATH_IROHA}/data/blockchain.network ]] && exit 3

# if iroha is restarting, wait until is has restarted
t=0
while [[ ${t} < 60 && -f ${PATH_IROHA}/import/sigterm ]]
do
  ((t+=1))
  sleep 1
done
[[ -f ${PATH_IROHA}/import/sigterm ]] && exit 4

CREATOR_ACCOUNT=${CREATOR_ACCOUNT:-$(<${PATH_IROHA}/data/name.peer)@$(<${PATH_IROHA}/data/blockchain.network)}
echo "Looking for account key file ${PATH_IROHA}/data/${CREATOR_ACCOUNT}.priv..."
t=0
while [[ ${t} < 60 && ! -f ${PATH_IROHA}/data/${CREATOR_ACCOUNT}.priv ]]
do
  ((t+=1))
  sleep 1
done
[[ ! -f ${PATH_IROHA}/data/${CREATOR_ACCOUNT}.priv ]] && exit 5

# start node process
su -m node
node -r esm ./app/main.js 2>&1
